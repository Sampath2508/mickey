package student;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccessException;
import com.adventnet.persistence.DataObject;
import com.adventnet.persistence.Persistence;
import com.adventnet.persistence.Row;
import com.adventnet.sym.server.mdm.util.MDMUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StudentJoin extends HttpServlet {

    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse res){
        RelationalAPI relapi = RelationalAPI.getInstance();
        Connection conn = null;
        try {
            PrintWriter out = res.getWriter();
            SelectQuery selectquery = new SelectQueryImpl(new Table("Users"));
            selectquery.addSelectColumn(new Column(null, "*"));
            selectquery.addJoin(
                    new Join("Users", "UserType", new String[]{"USER_ID"}, new String[]{"USER_ID"}, Join.INNER_JOIN)
            );

            selectquery.addJoin(
                    new Join("UserType", "Type", new String[] {"TYPE_ID"}, new String[] {"TYPE_ID"}, Join.INNER_JOIN)
             );
//
//            Persistence pers = MDMUtil.getPersistence();
//            DataObject DO = pers.get(selectquery);
//            Iterator Users =  DO.getRows("Users");
//            Iterator Type =  DO.getRows("Type");
//            Iterator UserType =  DO.getRows("UserType");
//
//            while (Type.hasNext()){
//                Row row = (Row) Type.next();
//                out.println(
//                        "\nType Id : " + row.get("TYPE_ID") +
//                        "\nType Name : " + row.get("TYPE_NAME")
//                );
//            }
//
//            while (UserType.hasNext()){
//                Row row = (Row) UserType.next();
//                out.println(
//                        "\nType Id : " + row.get("TYPE_ID") +
//                        "\nUser Id : " + row.get("USER_ID")
//                );
//            }
//
//            while (Users.hasNext()){
//                Row row = (Row) Users.next();
//                out.println(
//                        "\nUser Id : " + row.get("USER_ID") +
//                        "\nUserName : " + row.get("USERNAME") +
//                        "\nEmail : " + row.get("EMAIL") +
//                        "\nDepartment : " + row.get("DEPARTMENT") +
//                        "\nGender : " + row.get("GENDER")
//                );
//            }

            //Sql Query
            // select bs.book_name, bt.type_name, be.isbn, be.edition, be.year from bookstore as bs
                 //inner join booktype as bt on bs.type_id = bt.type_id
                 //inner join bookedition as be on bs.book_id = be.book_id;

//            SelectQuery selectquery = new SelectQueryImpl(new Table("BookStore"));
//            List<Column> colList = new ArrayList<Column>();
//            colList.add(new Column("BookStore", "BOOK_ID"));
//            colList.add(new Column("BookStore", "BOOK_NAME"));
//            colList.add(new Column("BookStore", "AUTHOR"));
//            colList.add(new Column("BookType", "TYPE_NAME"));
//            colList.add(new Column("BookEdition", "ISBN"));
//            colList.add(new Column("BookEdition", "EDITION"));
//            colList.add(new Column("BookEdition", "YEAR"));
//            selectquery.addSelectColumns(colList);
//            selectquery.addJoin(
//                    new Join("BookStore", "BookType", new String[] {"TYPE_ID"}, new String[] {"TYPE_ID"}, Join.INNER_JOIN)
//            );
//
//            selectquery.addJoin(
//                    new Join("BookStore", "BookEdition", new String[] {"BOOK_ID"}, new String[] {"BOOK_ID"}, Join.INNER_JOIN)
//            );

            conn = relapi.getConnection();
            DataSet data = relapi.executeQuery(selectquery , conn);
            out.println("User :");
            while(data.next()){
                out.println(
                        "\nTYPE ID : " + data.getAsString("TYPE_ID") +
                        "\nTYPE NAME : " + data.getAsString("TYPE_NAME") +
                        "\nUSER ID : " + data.getAsString("USER_ID") +
                        "\nUSERNAME : " + data.getAsString("USERNAME") +
                        "\nDEPARTMENT : " + data.getAsString("DEPARTMENT") +
                        "\nGENDER : " + data.getAsString("GENDER") +
                        "\nEMAIL : " + data.getAsString("EMAIL")
                );
            }
        } catch (Exception e) {
            System.out.println("------------------------------------------ Error ------------------------------------------");
            e.printStackTrace();
            System.out.println("------------------------------------------ ----- ------------------------------------------");
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
