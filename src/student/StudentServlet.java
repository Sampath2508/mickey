package student;

import com.adventnet.ds.query.*;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import com.adventnet.sym.server.mdm.util.MDMUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Iterator;

public class StudentServlet extends HttpServlet {

    private void printError(Exception e){
        System.out.println("============================== \"Error\" ==================================\n");
        System.out.println("Error | " + e.getClass() + " | " + e.getLocalizedMessage());
        e.printStackTrace();
        System.out.println("================================================================");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res){
        try {
            PrintWriter out = res.getWriter();
            Persistence pers = MDMUtil.getPersistence();
            Table Users = new Table("Users");

            SelectQuery sq = new SelectQueryImpl(Users);
            sq.addSelectColumn(new Column("Users", "*"));
//            sq.addSelectColumn(new Column("Users", "USER_ID"));
//            sq.addSelectColumn(new Column("Users", "USERNAME"));
            DataObject data = pers.get(sq);
            out.println("USER : ");
            Iterator rows = data.getRows("Users");
            while(rows.hasNext()) {
                Row row = (Row) rows.next();
                out.println(
                        "\nUser Id : " + row.get("USER_ID") +
                        "\nUsername : " + row.get("USERNAME") +
                        "\nPassword : " + row.get("PASSWORD") +
                        "\nEmail : " + row.get("EMAIL") +
                        "\nDepartment : " + row.get("DEPARTMENT") +
                        "\nGender : " + row.get("GENDER")
                );
            }
        } catch (Exception e) {
            printError(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res){
//        Row row = new Row("Users");
//        row.set("USERNAME", "ragu");
//        row.set("PASSWORD", "ragu123");
//        row.set("EMAIL", "ragu@fake.com");
//        row.set("DEPARTMENT", "CS");
//        row.set("GENDER", "Male");
//        DataObject DO = new WritableDataObject();
//        try {
//            DO.addRow(row);
//            DataAccess.add(DO);
//            res.getWriter().print("Row inserted..");
//        } catch (Exception e) {
//            printError(e);
//        }
        Row row = new Row("Users");
        row.set("USERNAME", "ragu");
        row.set("PASSWORD", "ragu123");
        row.set("EMAIL", "ragu@fake.com");
        row.set("DEPARTMENT", "Maths");
        row.set("GENDER", "Male");
        DataObject DO = new WritableDataObject();
        try {
            DO.addRow(row);
            Persistence pers = MDMUtil.getPersistence();
            pers.add(DO);
            res.getWriter().println("Row inserted...");
        } catch (DataAccessException | IOException e) {
            printError(e);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse res){
        try{
             Persistence pers = (Persistence) BeanUtil.lookup("Persistence");
             SelectQuery sq = new SelectQueryImpl(new Table("Users"));
             sq.addSelectColumn(new Column("Users", "*"));
             DataObject DO = pers.get(sq);
             Row row = DO.getRow("Users", new Criteria(new Column("Users", "USER_ID"), new Long(3), QueryConstants.EQUAL));
             row.set("DEPARTMENT", "CS");
             DO.updateRow(row);
             pers.update(DO);

//            UpdateQuery updatequery = new UpdateQueryImpl("Users");
//            updatequery.setCriteria(new Criteria(new Column("Users","USERNAME"), "ragu", QueryConstants.EQUAL)
//                    .and(new Criteria(new Column("Users", "EMAIL"), "ragu@fake.com", QueryConstants.LIKE)));
//            updatequery.setUpdateColumn("DEPARTMENT", "IT");
//            pers.update(updatequery);
            res.getWriter().print("Row Updated..");
        }catch (Exception e) {
            printError(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse res){
        try {
//            DeleteQuery dq = new DeleteQueryImpl("Users");
//            dq.setCriteria(new Criteria(new Column("Users", "USERNAME"), "ragu", QueryConstants.EQUAL));
//            DataAccess.delete(dq);
              Persistence pers = MDMUtil.getPersistence();
//              Row row = new Row("Users");
//              row.set("USER_ID", 2);
              pers.delete(new Criteria(new Column("Users", "USER_ID"), 2, QueryConstants.EQUAL));
              res.getWriter().println("Row Deleted..");

        }catch (Exception e){
            printError(e);
        }
    }
}
